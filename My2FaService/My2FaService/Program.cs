using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace My2FaService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {

					if (args.Length >= 2 && args[0].ToUpper() == "KESTEL")
					{
						Console.WriteLine(args);
						webBuilder.UseStartup<Startup>().UseKestrel(options =>
						{
							int port = 3131; // port defaults to 5443
							if (args.Length > 0)
								int.TryParse(args[1], out port); // args #1 is port

							/*if (args.Length > 2)
							{
								options.ListenAnyIP(port, listenOptions =>
								{
									listenOptions.UseHttps(args[1], args[2]); // NOTE: args #2 and #3 are path to cert-file.pfx and its password
								});
							}
							else
							{
							}*/

							options.ListenAnyIP(port);
							Console.WriteLine("PORT");
							Console.WriteLine(port);
						});
					}
					else
						webBuilder.UseStartup<Startup>();
				});
    }
}
