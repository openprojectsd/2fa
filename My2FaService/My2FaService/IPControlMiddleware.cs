﻿using System.Net;

namespace My2FaService
{
    public class IPControlMiddleware
    {
        readonly RequestDelegate _next;
        IConfiguration _configuration;
        static List<string> _ips = new List<string>();
        public IPControlMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            _configuration = configuration;
            _next = next;

            _ips = _configuration.GetSection("WhiteList").Get<List<string>>(); 
        }
        public async Task Invoke(HttpContext context)
        {
            try
            {

                //Client'ın IP adresini alıyoruz.
                IPAddress remoteIp = context.Connection.RemoteIpAddress;
                //Whitelist'te ki tüm IP'leri çekiyoruz.


                //Client IP, whitelist'te var mı kontrol ediyoruz.
                if (!_ips.Any(ip => IPAddress.Parse(ip).Equals(remoteIp)))
                {
                    //Eğer yoksa 403 hatası veriyoruz.
                    context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                    await context.Response.WriteAsync("No Access IP.");
                    return;
                }

                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}