﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using My2FaService.tfa;
using System.Linq;

namespace My2FaService.Controller
{
    [Route("")]
    [ApiController]
    public class tFA : ControllerBase
    {
        public static string Key = "asdsdfd!@#@!#$#5435/.,/,][";

        public static List<string> Applist { get; internal set; } = new List<string>();

        [HttpPost("Setup")]
        public ActionResult<SetupView> Setup(UserDto dto)
        {
            if(dto == null)
            {
                throw new ArgumentNullException(nameof(dto));
            }
            if (!Applist.Contains(dto.AppilcationCode))
            {
                return BadRequest(dto.AppilcationCode);
            }
            
            tfa.TwoFactorAuthenticator setup = new TwoFactorAuthenticator();

            var x = setup.GenerateSetupCode(dto.AppilcationName, dto.UserName, Key + dto.Id, 300, 300, true);
            if (x == null)
            {
                throw new Exception("Error Service");
            }
            SetupView setupView = new SetupView() { ManualEntryKey = x.ManualEntryKey, QrCodeSetupImageUrl = x.QrCodeSetupImageUrl };
            return Ok(setupView);
        }
         

        [HttpPost("Login")]
        public ActionResult< bool> Login(TfaDto dto)
        {
            try
            {
                if (dto == null)
                {
                    throw new ArgumentNullException(nameof(dto));
                }
                if (!Applist.Contains(dto.AppilcationCode))
                {
                    return BadRequest(dto.AppilcationCode);
                }
                tfa.TwoFactorAuthenticator setup = new TwoFactorAuthenticator();
                var x = setup.ValidateTwoFactorPIN(Key + dto.Id, dto.Password);
                return x;
            }
            catch (Exception ex)
            { 
                throw new Exception(string.Format("Error Service {0}",ex.Message));
            }
        }
    }

}
