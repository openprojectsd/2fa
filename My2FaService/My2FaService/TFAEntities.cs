﻿namespace My2FaService
{

    public class UserDto
    {
        public string AppilcationName { get; set; }= string.Empty;
        public string AppilcationCode { get; set; }= string.Empty;
        public string UserName { get; set; } = string.Empty;
        public string Id { get; set; } = string.Empty;
    }
    public class SetupView
    {

        public string ManualEntryKey { get; internal set; } = string.Empty;

        public string QrCodeSetupImageUrl { get; internal set; } = string.Empty;
    }
    public class TfaDto
    {
        public string AppilcationCode { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string Id { get; set; } = string.Empty;
    }
    public class SetupCode
    {
        public string Account { get; internal set; } = string.Empty;

        public string AccountSecretKey { get; internal set; } = string.Empty;

        public string ManualEntryKey { get; internal set; } = string.Empty;

        public string QrCodeSetupImageUrl { get; internal set; } = string.Empty;
    }
}
